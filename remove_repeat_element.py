#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
@File    :   remove_repeat_element.py
@Time    :   2024/02/02 17:02:32
@Author  :   longyu 
@Version :   1.0
@Desc    :   26. 删除有序数组中的重复项

'''
def remove_repeat_element(nums):
    """
    :type nums: List[int]
    :rtype: int
    """
    i, j = 0, 1
    while j < len(nums):
        if nums[j] == nums[i]:
            j += 1
            continue 
        nums[i+1] = nums[j]
        i += 1
        j += 1
    
    return i + 1
            

def main():
    nums = [1,1,2]
    remove_repeat_element(nums=nums)


if __name__ == '__main__':
    main()