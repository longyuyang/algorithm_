#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
@File    :   remove_element.py
@Time    :   2024/02/01 13:39:34
@Author  :   longyu 
@Version :   1.0
@Desc    :   27. 移除元素

'''


def removeElement(nums, val):
    """
    算法: 双指针，从头遍历到尾端
    考虑场景:
        1.移除元素后，数组为空
    """
    i, j = 0, len(nums) - 1
    default_element = -1
    ans = len(nums)
    while i <= j:
        if nums[i] != val:
            i += 1
            continue
        elif nums[j] == val:
            j -= 1
            ans -= 1
            continue
        
        if i == j:
            nums[i] = default_element
        else:
            nums[i] = nums[j]
        i += 1
        j -= 1
        ans -= 1   
    return ans
        

def main():
    nums = [3,2,2,3]
    val = 3
    ans = removeElement(nums=nums, val=val)
    print("ans:{}, nums:{}".format(ans, nums))


if __name__ == '__main__':
    main()